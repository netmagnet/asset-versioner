<?php

namespace Slts\AssetVersioner\Tests\Context;

use Mockery;
use Nette\Http\Request;
use Nette\Http\UrlScript;
use PHPUnit\Framework\TestCase;
use Slts\AssetVersioner\Context\NetteRequestContext;

class NetteRequestContextTest extends TestCase
{
    public function testIsSecure()
    {
        $request = Mockery::mock(Request::class)->shouldReceive('isSecured')->andReturn(true);

        $context = new NetteRequestContext($request->getMock()->makePartial());
        $this->assertTrue($context->isSecure());
    }

    public function testGetBasePath()
    {
        $request = Mockery::mock(Request::class)->shouldReceive('getUrl')->andReturnUsing(function () {
            return new UrlScript('http://example.com/');
        });

        $context = new NetteRequestContext($request->getMock()->makePartial());
        $this->assertEquals('', $context->getBasePath());
    }
}
