<?php

namespace Slts\AssetVersioner\Tests;

use Latte\Engine;
use Nette\Bridges\ApplicationDI\LatteExtension;
use Nette\Bridges\HttpDI\HttpExtension;
use Nette\DI\Compiler;
use Nette\DI\Container;
use Nette\DI\ContainerLoader;
use PHPUnit\Framework\TestCase;
use Slts\AssetVersioner\DI\AssetExtension;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\Packages;

class AssetExtensionTest extends TestCase
{
    private function createContainer(string $config): Container
    {
        $loader = new ContainerLoader(sys_get_temp_dir(), true);
        $class = $loader->load(function (Compiler $compiler) use ($config) {
            $compiler->addExtension('latte', new LatteExtension(sys_get_temp_dir() . '/latte', false));
            $compiler->addExtension('http', new HttpExtension());
            $compiler->addExtension('asset', new AssetExtension());
            $compiler->loadConfig($config);
        }, uniqid('', true));

        /** @var Container $container */
        $container = new $class();
        return $container;
    }

    public function testLoadConfigurationAllSetCorrectly()
    {
        $container = $this->createContainer(__DIR__ . '/configs/success.neon');

        /** @var Packages $packages */
        self::assertInstanceOf(Packages::class, $packages = $container->getService('asset.packages'));
        $p1 = $packages->getPackage('dynamic');
        self::assertInstanceOf(Package::class, $p1);
        $p2 = $packages->getPackage('fixUrl');
        self::assertInstanceOf(Package::class, $p2);
        $default = $packages->getPackage(null);
        self::assertSame($p1, $default);

        $latteFactory = $container->getByName('latte.latteFactory');
        /** @var Engine $engine */
        $engine = $latteFactory->create();
        self::assertInstanceOf(Engine::class, $engine);
        self::assertArrayHasKey('assetVersionerPackages', $engine->getProviders());
    }
}
