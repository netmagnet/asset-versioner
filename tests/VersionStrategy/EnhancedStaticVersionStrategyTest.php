<?php

namespace Slts\AssetVersioner\Tests\VersionStrategy;

use PHPUnit\Framework\TestCase;
use Slts\AssetVersioner\VersionStrategy\EnhancedStaticVersionStrategy;

class EnhancedStaticVersionStrategyTest extends TestCase
{
    public function testGetVersion()
    {
        $version = 'v1';
        $path = 'test-path';
        $enhancedStaticVersionStrategy = new EnhancedStaticVersionStrategy($version);
        $this->assertEquals($version, $enhancedStaticVersionStrategy->getVersion($path));
    }

    /**
     * @dataProvider getConfigs
     */
    public function testApplyVersion($path, $version, $format, $output)
    {
        $staticVersionStrategy = new EnhancedStaticVersionStrategy($version, $format);
        $this->assertEquals($output, $staticVersionStrategy->applyVersion($path));
    }

    public function getConfigs()
    {
        return [
            ['test-path', 'v1', null, 'test-path?v1'],
            ['test-path.css', '123', '_filename_._version_._extension_', 'test-path.123.css'],
            ['/base/test-path.css', '123', '_dirname_/_filename_._version_._extension_', '/base/test-path.123.css'],
            ['/base/test-path.css', '123', '_dirname_/_version_/_basename_', '/base/123/test-path.css'],
        ];
    }
}
