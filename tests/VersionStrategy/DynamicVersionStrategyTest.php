<?php

namespace Slts\AssetVersioner\Tests\VersionStrategy;

use PHPUnit\Framework\TestCase;
use Slts\AssetVersioner\VersionStrategy\DynamicVersionStrategy;

class DynamicVersionStrategyTest extends TestCase
{
    public function testGetVersion()
    {
        $path = 'test-path';
        $dynamicVersionStrategy = new DynamicVersionStrategy(__DIR__);
        $isMatch = preg_match('#\d+#', $dynamicVersionStrategy->getVersion($path));
        $this->assertEquals(1, $isMatch);
    }

    /**
     * @dataProvider getConfigs
     */
    public function testApplyVersion($path, $pattern, $format)
    {
        $dynamicVersionStrategy = new DynamicVersionStrategy(__DIR__, $format);
        $isMatch = preg_match($pattern, $dynamicVersionStrategy->applyVersion($path));
        $this->assertEquals(1, $isMatch);
    }

    public function getConfigs()
    {
        return [
            ['test-path', '#test-path#', null],
            ['test-path.css', '#test\-path\.[a-z0-9]+\.css#', '_filename_._version_._extension_'],
            ['/base/test-path.css', '#\/base\/test\-path\.[a-z0-9]+\.css#', '_dirname_/_filename_._version_._extension_'],
            ['/base/test-path.css', '#\/base\/[a-z0-9]+\/test\-path\.css#', '_dirname_/_version_/_basename_'],
        ];
    }
}
