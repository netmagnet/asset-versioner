# Asset Versioner

## Configuration
```
extensions:
    asset: Slts\AssetVersioner\DI\AssetExtension

asset:
    strategies:
        dynamic:
            class: Slts\AssetVersioner\VersionStrategy\DynamicVersionStrategy
            arguments:
                - "%wwwDir%"
        enhancedStatic:
            class: Slts\AssetVersioner\VersionStrategy\EnhancedStaticVersionStrategy
            arguments:
                - v2
                - "_dirname_/_filename_._version_._extension_"
    packages:
        dynamic:
            class: Symfony\Component\Asset\PathPackage
            arguments:
                - ""
                - @asset.strategy.dynamic
            default: true
        fixUrl:
            class: Symfony\Component\Asset\UrlPackage
            arguments:
                - 'http://www.netmagnet.cz'
                - @asset.strategy.enhancedStatic
```

## Usage
```
{* for PathPackage the start slash in path must be ommited, it is better to omit it everytime *}
{asset js/app.js} {* default package is used *}
{asset js/app.js fixUrl} {* fixUrl package is used *}
```
