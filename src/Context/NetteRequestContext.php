<?php

namespace Slts\AssetVersioner\Context;

use Nette\Http\Request;
use Symfony\Component\Asset\Context\ContextInterface;

class NetteRequestContext implements ContextInterface
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Gets the base path.
     *
     * @return string The base path
     */
    public function getBasePath(): string
    {
        return rtrim($this->request->getUrl()->basePath, '/');
    }

    /**
     * Checks whether the request is secure or not.
     *
     * @return bool true if the request is secure, false otherwise
     */
    public function isSecure(): bool
    {
        return $this->request->isSecured();
    }
}
