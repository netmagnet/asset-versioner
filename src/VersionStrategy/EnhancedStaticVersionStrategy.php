<?php

namespace Slts\AssetVersioner\VersionStrategy;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

class EnhancedStaticVersionStrategy implements VersionStrategyInterface
{
    private $version;
    private $format;

    public function __construct($version, string $format = null)
    {
        $this->version = $version;
        $this->format = $format ?: '_path_?_version_';
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion(string $path): string
    {
        return $this->version;
    }

    /**
     * {@inheritdoc}
     */
    public function applyVersion(string $path): string
    {
        $defaults = [
            'version' => $this->getVersion($path),
            'path' => $path,
            'dirname' => '',
            'basename' => '',
            'filename' => '',
            'extension' => '',
        ];
        $find = array_map(function ($key) {
            return "_{$key}_";
        }, array_keys($defaults));
        $replace = pathinfo($path) + $defaults;

        asort($find);
        ksort($replace);
        $trans = array_combine($find, $replace);

        $versionized = strtr(
            $this->format,
            $trans
        );

        if ($path && '/' === $path[0] && '/' !== $versionized[0]) {
            return '/'.$versionized;
        }

        return $versionized;
    }
}
