<?php

namespace Slts\AssetVersioner\VersionStrategy;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

class DynamicVersionStrategy implements VersionStrategyInterface
{
    protected $webDir;
    protected $format;

    public function __construct(string $webDir, $format = null)
    {
        $this->webDir = rtrim($webDir, '/') . '/';
        $this->format = $format ?? '_path_?_version_';
    }

    /**
     * Returns the asset version for an asset.
     *
     * @param string $path A path
     *
     * @return string The version string
     */
    public function getVersion(string $path): string
    {
        $version = $this->generateVersion($path);

        return substr(hash('md5', $version), 0, 10);
    }

    protected function generateVersion($path)
    {
        $path = ltrim($path, '/');
        $fullPath = $this->webDir . $path;
        if (!file_exists($fullPath)) {
            return $this->rand();
        }

        $time = @filemtime($fullPath);

        return $time ?: $this->rand();
    }

    protected function rand()
    {
        return random_int(10000, 99999);
    }

    /**
     * Applies version to the supplied path.
     *
     * @param string $path A path
     *
     * @return string The versionized path
     */
    public function applyVersion(string $path): string
    {
        $applicator = new EnhancedStaticVersionStrategy($this->getVersion($path), $this->format);
        $versionized = $applicator->applyVersion($path);
        if ($path && '/' === $path[0] && '/' !== $versionized[0]) {
            return '/' . $versionized;
        }

        return $versionized;
    }
}
