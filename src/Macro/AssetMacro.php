<?php

namespace Slts\AssetVersioner\Macro;

use Latte\CompileException;
use Latte\IMacro;
use Latte\MacroNode;
use Latte\PhpWriter;
use Latte\Runtime\Filters;
use Symfony\Component\Asset\Packages;

class AssetMacro implements IMacro
{
    const MACRO_NAME = 'asset';

    private $packages;

    public function __construct(Packages $packages)
    {
        $this->packages = $packages;
    }

    public function nodeOpened(MacroNode $node)
    {
        if ($node->prefix) {
            return false;
        }

        $path = $node->tokenizer->fetchWord();
        if (false === $path) {
            throw new CompileException("Missing filename in {$node->name}.");
        }
        $package = $node->tokenizer->fetchWord();
        if (false === $package) {
            $package = null;
        }
        
        $node->empty = true;
        $node->modifiers = '|safeurl|escape'; // auto-escape
        $writer = PhpWriter::using($node);
        if (preg_match('#^(["\']?)[^$\'"]*\1$#', $path)) { // Static path
            $path = trim($path, '"\'');
            $url = $this->packages->getUrl($path, $package);
            $url = Filters::safeUrl($url);
            $node->openingCode = $writer->write('<?php echo %escape(%var) ?>', $url);
        } else {
            $node->openingCode = $writer->write(
                '<?php echo %modify($this->global->assetVersionerPackages->getUrl(%0.word, %1.var)) ?>',
                $path,
                $package
            );
        }

        return true;
    }

    public function nodeClosed(MacroNode $node)
    {
    }

    public function initialize()
    {
    }

    public function finalize()
    {
    }
}
