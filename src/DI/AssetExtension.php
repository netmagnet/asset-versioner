<?php

namespace Slts\AssetVersioner\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Slts\AssetVersioner\Context\NetteRequestContext;
use Slts\AssetVersioner\Macro\AssetMacro;
use Symfony\Component\Asset\Packages;

class AssetExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        $builder = $this->getContainerBuilder();

        return Expect::structure([
            'debugMode' => Expect::bool($builder->parameters['debugMode']),
            'strategies' => Expect::arrayOf(Expect::structure([
                'class' => Expect::string(),
                'arguments' => Expect::array([])
            ])),
            'packages' => Expect::arrayOf(Expect::structure([
                'class' => Expect::string(),
                'arguments' => Expect::array([]),
                'default' => Expect::bool(false),
            ])),
        ]);
    }

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        $builder
            ->addDefinition($this->prefix('context'))
            ->setFactory(NetteRequestContext::class)
            ->setAutowired(true)
        ;

        $packagesDef = $builder
            ->addDefinition($this->prefix('packages'))
            ->setFactory(Packages::class)
        ;

        foreach ($this->config->strategies as $strategyName => $strategyConfig) {
            $builder
                ->addDefinition($this->prefix("strategy.{$strategyName}"))
                ->setFactory($strategyConfig->class, $strategyConfig->arguments)
                ->setAutowired(false)
            ;
        }

        foreach ($this->config->packages as $packageName => $packageConfig) {
            $packageDef = $builder
                ->addDefinition($this->prefix("package.{$packageName}"))
                ->setFactory($packageConfig->class, $packageConfig->arguments)
                ->setAutowired(false)
            ;
            $packagesDef
                ->addSetup('?->addPackage(?, ?)', ['@self', $packageName, $packageDef])
            ;
            if ($packageConfig->default) {
                $packagesDef
                    ->addSetup('?->setDefaultPackage(?)', ['@self', $packageDef])
                ;
            }
        }
    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        if ($builder->hasDefinition('latte.latteFactory')) {
            $packagesDef = $builder->getDefinition($this->prefix('packages'));
            $latte = $builder->getDefinition('latte.latteFactory')->getResultDefinition();
            $latte->addSetup('?->addProvider(?, ?)', ['@self', 'assetVersionerPackages', $packagesDef]);
            $latte->addSetup(
                '?->onCompile[] = function ($engine) { $engine->getCompiler()->addMacro("' . AssetMacro::MACRO_NAME . '", new ' . AssetMacro::class . '(?)); }',
                ['@self', $builder->getDefinition($this->prefix('packages'))]
            );
        }
    }
}
